﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class Spinner : MonoBehaviour
{
    private UICircle uiCircle;
    public GameObject buttonTextObject;
    public GameObject timerTextObject;
    private Text buttonText;
    private Text timerText;
    private bool spinning = false;

    int sfCounter = 0;
    int srCounter = 0;
    int arcRotator = 5;
    float timeTracker = 0f;

    private void Awake()
    {
        uiCircle = GetComponent<UICircle>();
        buttonText = buttonTextObject.GetComponent<Text>();
        timerText = timerTextObject.GetComponent<Text>();
    }

    private void SetSpinnerDefaults()
    {
        uiCircle.SetProgress(0);
    }

    void Start()
    {
        SetSpinnerDefaults();
    }

    public void StartSpinner()
    {
        if (!spinning)
        {
            buttonText.text = "STOP";
            StartCoroutine(SpinForward());
            spinning = true;
        }
        else
        {
            buttonText.text = "START";
            StopAllCoroutines();
            SetSpinnerDefaults();
            timeTracker = 0;
            spinning = false;
        }

    }

    private IEnumerator SpinForward()
    {
        uiCircle.SetInvertArc(true);
        uiCircle.SetArcRotation(90);
        float tick = 0;
        while (tick < 1)
        {
            tick += Time.deltaTime;
            timeTracker += Time.deltaTime;
            timerText.text = String.Format("{0:000.000}", timeTracker);
            uiCircle.SetProgress(tick);
            yield return null;
        }
        StartCoroutine(SpinReverse());
        sfCounter++;
    }

    private IEnumerator SpinReverse()
    {
        uiCircle.SetInvertArc(false);
        uiCircle.SetArcRotation(270);
        float tick = 1;
        while (tick > 0)
        {
            tick -= Time.deltaTime;
            timeTracker += Time.deltaTime;
            timerText.text = String.Format("{0:000.000}", timeTracker);
            uiCircle.SetProgress(tick);
            yield return null;
        }
        StartCoroutine(SpinForward());
        srCounter++;
    }
}
