#UICircleProgress
Contribution to opensource project UI Extensions. To see examples of the capabilities of the update to UICircle component, examine UICircleProgress scene.

![image](Examples/UICircleProgress/UIExtensions-UICircle.png)

This was a re-write of the UICircle component to utilize vertex streaming instead of quad stream.
